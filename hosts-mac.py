#coding:utf-8
import sys, urllib2,re
htmlH= urllib2.urlopen('http://www.360kb.com/kb/7_150.html')
html= htmlH.read()
reg= r'====更新分界线，复制下面内容到hosts文件即可=====.*#google hosts'
hostHtmlRe = re.search(reg, html, re.S)
hostHtml= hostHtmlRe.group()
hostHtml= hostHtml.replace('&nbsp;',' ')
hostHtml= hostHtml.replace('<span>', '')
hostHtml= hostHtml.replace('</span>', '')
hostHtml= hostHtml.replace('<pre class="highlight" data-highlight="highlight" data-lang="Ini">', '')
hostHtml= hostHtml.replace('<code class="Ini">', '')
hostHtml= hostHtml.replace('</p>','')
hostStr= hostHtml.replace('<br />','')

fp = open("/etc/hosts", 'r+')
hostOld = fp.read()
link = re.compile(r"#google hosts start==.*", re.S)
isFirst = hostOld.find("#google hosts start===")

if isFirst>0:
    hostNew=re.sub(link,'#google hosts start===========\r\n#'+hostStr,hostOld)
else:
    hostNew=hostOld+'#google hosts start===========\r\n#'+hostStr

print 'Update google hosts success.'
fp.seek(0)
fp.write(hostNew) #写入数据
fp.close() #关闭文件
